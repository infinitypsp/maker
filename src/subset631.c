/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "subset631.h"
#include "utility.h"
#include "psar.h"
#include "progressbar.h"
#include "vlfutility.h"

#include <infinity_kinstaller.h>
#include <pspdecrypt.h>

#include <string.h>

const char *g_631subset_go[] =
{
    "flash0:/kd/pspbtcnf_05g.bin",
    "flash0:/codepage/cptbl.dat",

    "flash0:/kd/sysmem.prx",
    "flash0:/kd/loadcore.prx",
    "flash0:/kd/exceptionman.prx",
    "flash0:/kd/interruptman.prx",
    "flash0:/kd/threadman.prx",
    "flash0:/kd/dmacman.prx",
    "flash0:/kd/iofilemgr.prx",
    "flash0:/kd/memlmd_05g.prx",
    "flash0:/kd/modulemgr.prx",
    "flash0:/kd/loadexec_05g.prx",
    "flash0:/kd/lowio.prx",
    "flash0:/kd/ge.prx",
    "flash0:/kd/idstorage.prx",
    "flash0:/kd/syscon.prx",
    "flash0:/kd/rtc.prx",
    "flash0:/kd/lfatfs.prx",
    "flash0:/kd/clockgen.prx",
    "flash0:/kd/display_05g.prx",
    "flash0:/kd/ctrl.prx",
    "flash0:/kd/led.prx",
    "flash0:/kd/power_05g.prx",
    "flash0:/kd/mediaman.prx",
    "flash0:/kd/eflash_05g.prx",
    "flash0:/kd/msstor.prx",
    "flash0:/kd/codepage.prx",
    "flash0:/kd/fatmsef.prx",
    "flash0:/kd/codec_05g.prx",
    "flash0:/kd/audio.prx",
    "flash0:/kd/hpremote_05g.prx",
    "flash0:/kd/openpsid.prx",
    "flash0:/kd/usb.prx",
    "flash0:/kd/wlan.prx",
    "flash0:/kd/wlanfirm_05g.prx",
    "flash0:/kd/registry.prx",
    "flash0:/kd/mgr.prx",
    "flash0:/kd/msaudio.prx",
    "flash0:/kd/chkreg.prx",
    "flash0:/kd/mesg_led_05g.prx",
    "flash0:/kd/semawm.prx",
    "flash0:/kd/amctrl.prx",
    "flash0:/kd/npdrm.prx",
    "flash0:/kd/iofilemgr_dnas.prx",
    "flash0:/kd/chnnlsv.prx",
    "flash0:/kd/utility.prx",
    "flash0:/kd/usb_host.prx",
    "flash0:/kd/usbbsmcdc.prx",
    "flash0:/kd/input_05g.prx",
    "flash0:/kd/hidsvc.prx",
    "flash0:/kd/padsvc.prx",
    "flash0:/kd/bsman.prx",
    "flash0:/kd/btdun.prx",
};

#define SUBSET_GO_MAIN_N   (sizeof(g_631subset_go)/sizeof(char *))


const char *g_631subset_headers_go[] =
{    
    "flash0:/kd/me_wrapper.prx",
    "flash0:/kd/vaudio.prx",
    "flash0:/kd/impose_05g.prx",
    "flash0:/kd/avcodec.prx",
    "flash0:/kd/vshbridge.prx",
    "flash0:/kd/mediasync.prx",
    "flash0:/kd/usersystemlib.prx",
    "flash0:/kd/libatrac3plus.prx",
    "flash0:/vsh/module/paf.prx",
    "flash0:/vsh/module/common_gui.prx",
    "flash0:/vsh/module/common_util.prx",
    "flash0:/vsh/module/vshmain.prx"
};

#define SUBSET_GO_HEADER_N   (sizeof(g_631subset_headers_go)/sizeof(char *))

const char *g_631subset[] =
{
    "flash0:/kd/pspbtcnf.bin",
    "flash0:/kd/pspbtcnf_02g.bin",
    "flash0:/kd/pspbtcnf_03g.bin",
    "flash0:/kd/pspbtcnf_04g.bin",
    "flash0:/kd/pspbtcnf_07g.bin",
    "flash0:/kd/pspbtcnf_09g.bin",
    
    "flash0:/codepage/cptbl.dat",
    
    "flash0:/kd/sysmem.prx",
    "flash0:/kd/loadcore.prx",
    "flash0:/kd/exceptionman.prx",
    "flash0:/kd/interruptman.prx",
    "flash0:/kd/threadman.prx",
    "flash0:/kd/dmacman.prx",
    "flash0:/kd/iofilemgr.prx",
    "flash0:/kd/memlmd_01g.prx",
    "flash0:/kd/memlmd_02g.prx",
    "flash0:/kd/memlmd_03g.prx",
    "flash0:/kd/memlmd_04g.prx",
    "flash0:/kd/memlmd_07g.prx",
    "flash0:/kd/memlmd_09g.prx",
    "flash0:/kd/modulemgr.prx",
    
    "flash0:/kd/loadexec_01g.prx",
    "flash0:/kd/loadexec_02g.prx",
    "flash0:/kd/loadexec_03g.prx",
    "flash0:/kd/loadexec_04g.prx",
    "flash0:/kd/loadexec_07g.prx",
    "flash0:/kd/loadexec_09g.prx",
    "flash0:/kd/lowio.prx",
    "flash0:/kd/ge.prx",
    "flash0:/kd/idstorage.prx", 
    "flash0:/kd/syscon.prx",
    "flash0:/kd/rtc.prx",
    "flash0:/kd/lfatfs.prx", 
    "flash0:/kd/clockgen.prx", 
    "flash0:/kd/mediaman.prx", 
    "flash0:/kd/ata.prx", 
    "flash0:/kd/umdman.prx", 
    "flash0:/kd/umd9660.prx", 
    "flash0:/kd/isofs.prx", 
    "flash0:/kd/display_01g.prx",
    "flash0:/kd/display_02g.prx",
    "flash0:/kd/display_03g.prx",
    "flash0:/kd/display_04g.prx",
    "flash0:/kd/display_07g.prx",
    "flash0:/kd/display_09g.prx",
    "flash0:/kd/ctrl.prx", 
    "flash0:/kd/led.prx", 
    "flash0:/kd/power_01g.prx",
    "flash0:/kd/power_02g.prx",
    "flash0:/kd/power_03g.prx",
    "flash0:/kd/power_04g.prx",
    "flash0:/kd/power_07g.prx",
    "flash0:/kd/power_09g.prx",
    "flash0:/kd/msstor.prx", 
    "flash0:/kd/codepage.prx", 
    "flash0:/kd/fatms.prx",    
    "flash0:/kd/codec_01g.prx", 
    "flash0:/kd/codec_02g.prx",
    "flash0:/kd/codec_03g.prx",
    "flash0:/kd/codec_04g.prx",
    "flash0:/kd/codec_07g.prx",
    "flash0:/kd/codec_09g.prx",
    "flash0:/kd/audio.prx", 
    "flash0:/kd/hpremote_01g.prx",
    "flash0:/kd/hpremote_02g.prx",
    "flash0:/kd/hpremote_03g.prx",
    "flash0:/kd/hpremote_04g.prx",
    "flash0:/kd/hpremote_07g.prx",
    "flash0:/kd/hpremote_09g.prx",
    "flash0:/kd/openpsid.prx", 
    "flash0:/kd/usb.prx",
    "flash0:/kd/wlan.prx", 
    "flash0:/kd/wlanfirm_01g.prx", 
    "flash0:/kd/wlanfirm_02g.prx",
    "flash0:/kd/wlanfirm_03g.prx",
    "flash0:/kd/wlanfirm_04g.prx",
    "flash0:/kd/wlanfirm_07g.prx",
    "flash0:/kd/wlanfirm_09g.prx",
    "flash0:/kd/registry.prx", 
    "flash0:/kd/mgr.prx",
    "flash0:/kd/msaudio.prx",
    "flash0:/kd/chkreg.prx",
    "flash0:/kd/mesg_led_01g.prx", 
    "flash0:/kd/mesg_led_02g.prx",
    "flash0:/kd/mesg_led_03g.prx",
    "flash0:/kd/mesg_led_04g.prx",
    "flash0:/kd/mesg_led_07g.prx",
    "flash0:/kd/mesg_led_09g.prx",
    "flash0:/kd/semawm.prx",
    "flash0:/kd/amctrl.prx",
    "flash0:/kd/npdrm.prx",
    "flash0:/kd/iofilemgr_dnas.prx",
    "flash0:/kd/chnnlsv.prx",
    "flash0:/kd/utility.prx",
};

#define SUBSET_MAIN_N   (sizeof(g_631subset)/sizeof(char *))

const char *g_631subset_headers[] =
{    
    "flash0:/kd/me_wrapper.prx", 
    "flash0:/kd/vaudio.prx",
    "flash0:/kd/impose_01g.prx",
    "flash0:/kd/impose_02g.prx",
    "flash0:/kd/impose_03g.prx",
    "flash0:/kd/impose_04g.prx",
    "flash0:/kd/impose_07g.prx",
    "flash0:/kd/impose_09g.prx",
    "flash0:/kd/avcodec.prx",
    "flash0:/kd/vshbridge.prx",
    "flash0:/kd/mediasync.prx",
    "flash0:/kd/usersystemlib.prx",
    "flash0:/kd/libatrac3plus.prx",
    "flash0:/vsh/module/paf.prx",
    "flash0:/vsh/module/common_gui.prx",
    "flash0:/vsh/module/common_util.prx",
    "flash0:/vsh/module/vshmain.prx",
};

#define SUBSET_HEADER_N   (sizeof(g_631subset_headers)/sizeof(char *))

typedef struct
{
    const char **main;
    int main_n;
    const char **headers;
    int header_n;
} SubsetFirmware;

SubsetFirmware g_go_subset =
{
    .main = g_631subset_go,
    .main_n = SUBSET_GO_MAIN_N,
    .headers = g_631subset_headers_go,
    .header_n = SUBSET_GO_HEADER_N,
};

SubsetFirmware g_std_subset =
{
    .main = g_631subset,
    .main_n = SUBSET_MAIN_N,
    .headers = g_631subset_headers,
    .header_n = SUBSET_HEADER_N,
};

static int g_start_percentage = 0, g_end_percentage = 0;
u8 *g_work_buffer = NULL;
int g_work_buffer_size = 0;
static int g_number_of_files = 0;

SubsetFirmware *get_subset(void)
{
    if (isPspGo())
    {
        return &g_go_subset;
    }
    else
    {
        return &g_std_subset;
    }
}
static int pack_631(const char *filename)
{
    char path[256];
    strcpy(path, "flash0:/");
    strcat(path, filename+strlen("psar0:/flash0/"));
    
    int i;
    for (i = 0; i < get_subset()->main_n; ++i)
    {
        if (strcmp(path, get_subset()->main[i]) != 0)
        {
            continue;
        }
        
        int res = ReadFile(filename, 0, g_work_buffer, g_work_buffer_size);
    
        if (res < 0)
        {
            return res;
        }
        
        int signcheck = isSigncheckFilePSAR(filename);
        
        if (signcheck < 0)
        {
            return res;
        }
        
        res = mfcAddFile(path, g_work_buffer, res, getModelsForFilePSAR(filename), signcheck);
        
        if (res < 0)
        {
            return res;
        }
        
        // update progress bar
        SetProgressBarPercentage(g_start_percentage+(((g_end_percentage-g_start_percentage)*g_number_of_files)/(SUBSET_MAIN_N+SUBSET_HEADER_N)), 0);
        g_number_of_files++;
    }
    
    for (i = 0; i < get_subset()->header_n; ++i)
    {
        if (strcmp(path, get_subset()->headers[i]) != 0)
        {
            continue;
        }
        
        // minimum size to pass btcnf hash check 0x161
        int res = ReadFile(filename, 0, g_work_buffer, 0x161);
    
        if (res < 0)
        {
            return res;
        }
        
        int signcheck = isSigncheckFilePSAR(filename);
        
        if (signcheck < 0)
        {
            return res;
        }

        res = mfcAddFile(path, g_work_buffer, res, getModelsForFilePSAR(filename), signcheck);
        
        if (res < 0)
        {
            return res;
        }
        
        // update progress bar
        SetProgressBarPercentage(g_start_percentage+(((g_end_percentage-g_start_percentage)*g_number_of_files)/(SUBSET_MAIN_N+SUBSET_HEADER_N)), 0);
        g_number_of_files++;
    }
    
    return 0;
}

int pack_631_ipl(const char *filename)
{
    char file[256];
    strcpy(file, filename+strlen("flash0:/ipl"));
    
    int res = ReadFile(filename, 0, g_work_buffer, g_work_buffer_size);

    if (res < 0)
    {
        return res;
    }
    
    int model = getModelsForFilePSAR(filename);
    
    if (model != MODEL(0))
    {
        // need to decrypt IPL
        res = pspDecryptPRX(g_work_buffer, g_work_buffer, res);
        
        if (res < 0)
        {
            return res;
        }
    }
    
    res = mfcAddFile(file, g_work_buffer, res, model, 0);
    
    if (res < 0)
    {
        return res;
    }
    
    return 0;
}

static void pack_631_subset(void)
{
    int res = ReadDirectory("psar0:/flash0", pack_631);
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X packing 6.31 firmware subset.", res);
    }
}

static void pack_631_ipls(void)
{
    int res = ReadDirectory("psar0:/ipl", pack_631_ipl);
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X packing 6.31 ipl subset.", res);
    }
}

void PackSubset(int start_percentage, int end_percentage, u8 *work_buffer, int size)
{
    g_start_percentage = start_percentage;
    g_end_percentage = end_percentage;
    g_work_buffer = work_buffer;
    g_work_buffer_size = size;
    
    SetProgressBarStatus("Packing 6.31 subset...");
	   
    int res = MountPSAR("631.PBP");
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X mounting 6.31 updater.", res);
    }
    
 	if (mfcInitSection("6.31") < 0)
	{
		ErrorExit("Error creating \"6.31\" section.");
	}
   
    pack_631_subset();  
    
    if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"6.31\" section.");
	}
  
 	if (mfcInitSection("IPL") < 0)
	{
		ErrorExit("Error creating \"IPL\" section.");
	}
    
    pack_631_ipls();
    
    if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"IPL\" section.");
	}
    
    res = UnmountPSAR();
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X unmounting 6.31 updater.", res);
    }
}
