/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "reboot.h"
#include "utility.h"

#include <pspsdk.h>
#include <psploadcore.h>
#include <pspdecrypt.h>
#include <string.h>

static int find_reboot(u8 *input, u8 *output, int size)
{
	int i;

	for (i = 0; i < (size - sizeof(PSP_Header)); i++)
	{
        PSP_Header *header = (PSP_Header *)(input+i);
        
        // check for ~PSP signature
		if (memcmp(input+i, "~PSP", 4) == 0)
		{
            // this is the reboot, move to output
			int size = header->psp_size;
			memmove(output, input+i, size);
			return size;
		}
	}

	return -1;
}

RebootErrors ExtractReboot(const char *filename, u8 *work_buffer, int size, REBOOT_EXTRACTION_HANDLER handler)
{
    // read in the loadexec file
	int exec_size = ReadFile(filename, 0, work_buffer, size);

	if (exec_size < 0)
    {
        return REBOOT_READ_ERR;
    }

    // decrypt loadexec so we can search within it
	exec_size = pspDecryptPRX(work_buffer, work_buffer, exec_size);
    
	if (exec_size < 0)
	{
        return REBOOT_DECRYPT_EXEC_ERR;
	}
    
    // align to 64
    while (exec_size % 64) exec_size++;
    
    // loadexec is compressed, so do decompression too
	int decompressed_size = pspDecompress(work_buffer, work_buffer+exec_size, size-exec_size);
    
	if (decompressed_size < 0)
	{
        return REBOOT_DECOMPRESS_EXEC_ERR;
	}

	int reboot_size = find_reboot(work_buffer+exec_size, work_buffer, decompressed_size);
    
	if (reboot_size < 0)
	{
        return REBOOT_FIND_ERR;
	}

    // decrypt the reboot.bin
	reboot_size = pspDecryptPRX(work_buffer, work_buffer, reboot_size);
    
	if (reboot_size < 0)
	{
        return REBOOT_DECRYPT_ERR;
	}

	reboot_size = pspDecompress(work_buffer, work_buffer+exec_size, size-exec_size);
    
	if (reboot_size < 0)
	{
        return REBOOT_DECOMPRESS_ERR;
	}
    
    handler(filename, work_buffer+exec_size, reboot_size);
    
    // ALL OK
    return REBOOT_NO_ERR;
}
