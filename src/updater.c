/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "updater.h"

#include <pspiofilemgr.h>
#include <pspdecrypt.h>
#include <string.h>

#define PBP_HEADER_MAGIC    (0x50425000)

static HandlingResult process_if_xor_module(PSP_Header *updater, UPDATER_PRX_EXTRACTION_HANDLER handler)
{
    u32 key;
    u32 magic = updater->signature;
    
    for (key = 0; key <= 0xFF; key += 0x01) 
    {
        u32 xorkey = (key * 0x1000000) + (key * 0x10000) + (key * 0x100) + key;

        // check if ~PSP module
        if ((magic ^ xorkey) == 0x5053507E)
        {
            if (xorkey != 0)
            {
                // apply xor to entire file
                int k;
                int size = (updater->psp_size ^ xorkey);
                for (k = 0; k < size; ++k)
                {
                    ((unsigned char *)updater)[k] ^= xorkey;
                }
            }
            
            // found module, return to handler
            return handler(updater, updater->psp_size, MODE_XOR);
        }
    }
    
    return NEXT;
}

static void updater_demangle_xor(char *buffer, int size, unsigned char a2, unsigned char a3, unsigned char t0)
{
    unsigned char v1 = a2;
    unsigned char t1 = a3;
    unsigned char a0 = t0 + 1;
    int i;
    for (i = 0; i < size; i++)
    {
        unsigned char tmp = t1 % a0;
        v1 += t1;
        buffer[i] ^= v1;
        t1 = t0;
        t0 = v1;
        v1 = tmp;
        a0 = t0 + 1;
    }
}

static HandlingResult process_if_mangled_module(PSP_Header *updater, UPDATER_PRX_EXTRACTION_HANDLER handler)
{
    typedef struct
    {
        u8 a;
        u8 b;
        u8 c;
    } ModuleMangleSeeds;
    
    ModuleMangleSeeds seeds[] = 
    {
        { 0x07, 0x05, 0x13 },
        { 0x07, 0x09, 0x13 },
        { 0x19, 0x01, 0x0D },
        { 0x0B, 0x07, 0x06 },
        { 0x0F, 0x01, 0x0D },
    };

    int k;
    for (k = 0; k < sizeof(seeds)/sizeof(ModuleMangleSeeds); ++k)
    {
        unsigned char a = seeds[k].a;
        unsigned char b = seeds[k].b;
        unsigned char c = seeds[k].c;
        
        // create the xorkey for the first 4 bytes of these key seeds
        unsigned int xorkey = 0;
        updater_demangle_xor((char *)&xorkey, 4, a, b, c);
        
        // check if we have a ~PSP module
        if ((updater->signature ^ xorkey) == 0x5053507E)
        {
            PSP_Header header;
            memcpy((char *)&header, (char *)updater, sizeof(PSP_Header));
            
            // decrypt header
            updater_demangle_xor((char *)&header, sizeof(PSP_Header), a, b, c);
            
            // decrypt entire module with header info
            updater_demangle_xor((char *)updater, header.psp_size, a, b, c);
            
            // give to handler
            return handler(updater, updater->psp_size, MODE_MANGLED);
        }
    }
    
    return NEXT;
}

static HandlingResult process_if_encrypted_module(PSP_Header *updater, UPDATER_PRX_EXTRACTION_HANDLER handler)
{
    int k;
    
    if (updater->_80 != 0x80 || updater->reserved2[0] != 0 || updater->reserved2[1] != 0)
        return NEXT;
        
    // this module is double encrypted so we're looking for a module without header
    if (updater->signature == 0x5053507E)
        return NEXT;
        
    // check for no signcheck
    for (k = 0; k < 0x58/4; ++k)
    {
        if (((u32 *)updater->scheck)[k] != 0)
            return NEXT;
    }
    
    // now attempt to decrypt (expensive)
    int res = pspDecryptPRX((u8 *)updater, (u8 *)updater, updater->comp_size + sizeof(PSP_Header));
    
    if (res >= 0)
    {
        // give to handler
        return handler(updater, updater->psp_size, MODE_ENCRYPTED);
    }
    
    return NEXT;
}

int ExtractUpdaterPRX(const char *filename, char *work_buffer, int size, UPDATER_PRX_EXTRACTION_HANDLER handler)
{
    unsigned int prx_size = size;
	u32 pbp_header[0x28/4];
    
    // check we have a 64 byte aligned buffer. this is important as kirk requires it
    if ((u32)work_buffer % 64)
    {
        // not 64 byte aligned
        return -1;
    }
    
	SceUID fd = sceIoOpen(filename, PSP_O_RDONLY, 0);

	if (fd < 0)
	{
        return fd;
	}
    
    // read in header to check if PBP type
    int res = sceIoRead(fd, pbp_header, sizeof(pbp_header));
    
    if (res != sizeof(pbp_header))
    {
        // error reading header
        sceIoClose(fd);
        return -2;
    }
    
    if (pbp_header[0] == PBP_HEADER_MAGIC)
    {
        // we need to seek to the PRX and read it
        prx_size = pbp_header[0x24/4] - pbp_header[0x20/4];
        sceIoLseek(fd, pbp_header[0x20/4], PSP_SEEK_SET);
    }
    
    // read PRX
	if ((prx_size = sceIoRead(fd, work_buffer, prx_size)) < 0)
	{
        sceIoClose(fd);
        return prx_size;
	}
    
    sceIoClose(fd);
    
    // use prxdecrypt to decrypt it
	if ((prx_size = pspDecryptPRX((u8 *)work_buffer, (u8 *)work_buffer, prx_size)) < 0)
	{
        return prx_size;
	}
    
    int i;
    int do_xor = 1, do_mangle = 1, do_encrypt = 1;
    
    for (i = 0; i < size - sizeof(PSP_Header) && (do_xor || do_mangle || do_encrypt); i += 0x40) 
    {
        PSP_Header *prx = (PSP_Header *)(work_buffer + i);
        
        if (do_xor)
        {
            // some modules are hidden by 8-bit XOR
            HandlingResult res = process_if_xor_module(prx, handler);
            
            if (res == STOP)
            {
                break;
            }
            else if (res == STOP_MODE || res == CONTINUE)
            {
                if (res == STOP_MODE)
                    do_xor = 0;
                    
                continue;
            }
        }
        
        if (do_mangle)
        {
            // some modules are mangled
            HandlingResult res = process_if_mangled_module(prx, handler);
            
            if (res == STOP)
            {
                break;
            }
            else if (res == STOP_MODE || res == CONTINUE)
            {
                if (res == STOP_MODE)
                    do_mangle = 0;
                    
                continue;
            }
        }
        
        if (do_encrypt)
        {
            // some modules are encrypted
            HandlingResult res = process_if_encrypted_module(prx, handler);
            
            if (res == STOP)
            {
                break;
            }
            else if (res == STOP_MODE || res == CONTINUE)
            {
                if (res == STOP_MODE)
                    do_encrypt = 0;
                    
                continue;
            }
        }
    }
    
    return 0;
}
