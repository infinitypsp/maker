/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#ifndef UTILITY_H_
#define UTILITY_H_

#include <psptypes.h>
#include <pspmodulemgr.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef int (* DIRECTORY_READ_HANDLER)(const char *filename);

int ReadFile(const char *file, int seek, void *buf, int size);
int WriteFile(char *file, void *buf, int size);

int ReadDirectory(const char *directory, DIRECTORY_READ_HANDLER handler);

int VerifyFileSHA1(const char *filename, const unsigned char *check, u8 *work_buffer, int size);

SceUID LoadStartModuleBuffer(void *buffer, unsigned int size);
int LoadStartModule(const char *filename);

int isPspGo();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // UTILITY_H_
