/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "progressbar.h"
#include "vlfutility.h"
#include "utility.h"
#include "hybrid.h"
#include "wave.h"
#include "background.h"

#include <pspkernel.h>
#include <pspsdk.h>
#include <pspgum.h>

#include <vlf.h>

PSP_MODULE_INFO("infinity_maker", 0x0800, 1, 0);
PSP_MAIN_THREAD_ATTR(PSP_THREAD_ATTR_VSH);

static char *g_main_menu[] =
{
    "Build Hybrid Firmware",
    "Exit"
};

#define MAIN_MENU_ITEM_N    (sizeof(g_main_menu)/sizeof(char *))

static int onMainMenuSelection(int sel)
{
	switch (sel)
	{
		case 0:
        {
            InitProgressBar(136);

            SceUID thid = sceKernelCreateThread("builder_thread", (SceKernelThreadEntry)&BuildHybridFirmware, 0x40, 0x10000, 0, NULL);

            if (thid >= 0)
            {
                sceKernelStartThread(thid, 0, NULL);
            }
            else
            {
                ErrorExit("could not create thread!\n");
            }

            break;
        }

		case 1:
            sceKernelExitGame();
            break;
	}

	return VLF_EV_RET_REMOVE_OBJECTS | VLF_EV_RET_REMOVE_HANDLERS;
}

int app_main(int argc, char *argv[])
{
    // setup VLF
    vlfGuiSystemSetup(1, 1, 1);
    vlfGuiSetModel(g_gmoWave, size_g_gmoWave);
    vlfGuiSetModelSpeed(1.f/45.f);

    // if we're on PSP 3000 based devices we need to read 3g version of bmp
    vlfGuiSetBackgroundFileBuffer(g_background, size_g_background, 1);

    ScePspFMatrix4 matrix;
    ScePspFVector3 scale;
    ScePspFVector3 translate;
    translate.x = 0.f;
    translate.y = -13.5f;
    translate.x = 0.f;
    scale.x = scale.y = scale.z = 8.5f;
    gumLoadIdentity(&matrix);
    gumScale(&matrix, &scale);
    //gumRotateZ(&matrix, angle);
    gumTranslate(&matrix, &translate);
    vlfGuiSetModelWorldMatrix(&matrix);

    VlfText title_bar = vlfGuiAddText(0, 0, "6.61 Infinity Firmware Builder");
    VlfPicture update_icon = vlfGuiAddPictureResource("update_plugin", "tex_update_icon", 0, 0);

    // set our title bar
    vlfGuiSetTitleBar(title_bar, update_icon, 1, 0);

    // use a central menu for this
	vlfGuiCentralMenu(MAIN_MENU_ITEM_N, g_main_menu, 0, onMainMenuSelection, 0, 0);

	while (1)
	{
		vlfGuiDrawFrame();
	}

    return 0;
}
