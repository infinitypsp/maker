/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "subset631.h"
#include "utility.h"
#include "psar.h"
#include "reboot.h"
#include "progressbar.h"
#include "vlfutility.h"

#include <string.h>

#include <infinity_kinstaller.h>

static int g_start_percentage661 = 0, g_end_percentage661 = 0;
u8 *g_work_buffer661 = NULL;
int g_work_buffer_size661 = 0;
static int g_number_of_files_661 = 0, g_current_file_n = 0;

static int count_files(const char *filename)
{
    g_number_of_files_661++;
    return 0;
}

void pack_reboot(const char *filename, const u8 *reboot_bin, unsigned int size)
{
    char path[256];
    strcpy(path, "flash0:/kn/reboot_");
    strcat(path, filename+strlen("psar0:/flash0/kd/loadexec_"));
    strcpy(strchr(path, '.'), ".bin");
    
    int res = mfcAddFile(path, (u8 *)reboot_bin, size, getModelsForFilePSAR(filename), 0);
    
    if (res < 0)
    {
       ErrorExit("Error 0x%08X packing %s.", path);
    }
}

int pack_661(const char *filename)
{
    char path[256];
    strcpy(path, "flash0:/");
    strcat(path, filename+strlen("psar0:/flash0/"));
    
    if (strncmp(path, "flash0:/kd", strlen("flash0:/kd")) == 0)
    {
        path[strlen("flash0:/kd")-1] = 'n';
    }
    else if (strncmp(path, "flash0:/vsh", strlen("flash0:/vsh")) == 0)
    {
        path[strlen("flash0:/vsh")-1] = 'n';
    }
    
    int res = ReadFile(filename, 0, g_work_buffer661, g_work_buffer_size661);
    
    if (res < 0)
    {
        return res;
    }
    
    int signcheck = isSigncheckFilePSAR(filename);
    
    if (signcheck < 0)
    {
        return res;
    }
    
    res = mfcAddFile(path, g_work_buffer661, res, getModelsForFilePSAR(filename), signcheck);
    
    if (res < 0)
    {
        return res;
    }
    
    // check if loadexec_XXg
    if (strncmp(filename, "psar0:/flash0/kd/loadexec_", strlen("psar0:/flash0/kd/loadexec_")) == 0)
    {
        RebootErrors error = ExtractReboot(filename, g_work_buffer661, g_work_buffer_size661, pack_reboot);
        
        if (error != REBOOT_NO_ERR)
        {
            ErrorExit("Error %i extracting reboot.bin", error);
        }
    }
    
    SetProgressBarPercentage(g_start_percentage661+(((g_end_percentage661-g_start_percentage661)*g_current_file_n)/(g_number_of_files_661)), 0);
    g_current_file_n++;
    return 0;
}

static int pack_661_firmware(void)
{
    g_number_of_files_661 = 0;
    g_current_file_n = 0;
    ReadDirectory("psar0:/flash0", count_files);
    
    int res = ReadDirectory("psar0:/flash0", pack_661);
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X packing 6.61 firmware.", res);
    }

    return 0;
}

void PackFirmware(int start_percentage, int end_percentage, u8 *work_buffer, int size)
{
    g_start_percentage661 = start_percentage;
    g_end_percentage661 = end_percentage;
    g_work_buffer661 = work_buffer;
    g_work_buffer_size661 = size;
    
    SetProgressBarStatus("Packing 6.61 firmware...");
    
 	if (mfcInitSection("6.61") < 0)
	{
		ErrorExit("Error creating \"6.61\" section.");
	}
	   
    int res = MountPSAR("661.PBP");
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X mounting 6.61 updater.", res);
    }
    
    pack_661_firmware();
    
    res = UnmountPSAR();
    
    if (res < 0)
    {
        ErrorExit("Error 0x%08X unmounting 6.61 updater.", res);
    }    
    
    if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"6.61\" section.");
	}
}
