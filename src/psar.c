/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "psar.h"

#include <pspsdk.h>
#include <pspiofilemgr.h>
#include <string.h>

#define PSAR_CTL_CMD_MOUNT          (1)
#define PSAR_CTL_CMD_UMOUNT         (2)

#define PSAR_IOCTL_CMD_SIGNCHECK    (1)
#define PSAR_IOCTL_CMD_MODELS       (2)

int MountPSAR(const char *filename)
{
    return sceIoDevctl("psar0:", PSAR_CTL_CMD_MOUNT, (u8 *)filename, strlen(filename)+1, NULL, 0);
}

int UnmountPSAR(void)
{
    return sceIoDevctl("psar0:", PSAR_CTL_CMD_UMOUNT, NULL, 0, NULL, 0);
}

int isSigncheckFilePSAR(const char *filename)
{
    // this is only valid for psar0 access
    if (strncmp(filename, "psar0:", 6) != 0)
    {
        return -1;
    }
    
    SceUID fd = sceIoOpen(filename, PSP_O_RDONLY, 0);
    
	if (fd < 0)
		return fd;
        
   int signcheck = 0;
   int res = sceIoIoctl(fd, PSAR_IOCTL_CMD_SIGNCHECK, NULL, 0, &signcheck, 4);
   
   sceIoClose(fd);
   
   if (res < 0)
    return res;
    
   return signcheck ? (1) : (0);    
}

int getModelsForFilePSAR(const char *filename)
{
    // this is only valid for psar0 access
    if (strncmp(filename, "psar0:", 6) != 0)
    {
        return -1;
    }
    
	SceUID fd = sceIoOpen(filename, PSP_O_RDONLY, 0);
    
	if (fd < 0)
		return fd;
        
   int models = 0;
   int res = sceIoIoctl(fd, PSAR_IOCTL_CMD_MODELS, NULL, 0, &models, 4);
   
   sceIoClose(fd);
   
   if (res < 0)
    return res;
    
   return models;
}
