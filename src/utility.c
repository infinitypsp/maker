/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "utility.h"

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspiofilemgr.h>
#include <string.h>

#include <infinity_kinstaller.h>

int ReadFile(const char *file, int seek, void *buf, int size)
{
	SceUID fd = sceIoOpen(file, PSP_O_RDONLY, 0);
	if (fd < 0)
		return fd;

	if (seek > 0)
	{
		if (sceIoLseek(fd, seek, PSP_SEEK_SET) != seek)
		{
			sceIoClose(fd);
			return -1;
		}
	}

	int read = sceIoRead(fd, buf, size);
	
	sceIoClose(fd);
	return read;
}

int WriteFile(char *file, void *buf, int size)
{
	SceUID fd = sceIoOpen(file, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
	
	if (fd < 0)
	{
		return fd;
	}

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}


int ReadDirectory(const char *directory, DIRECTORY_READ_HANDLER handler)
{
    char newfile[256];
    static struct SceIoDirent g_dirent; 
	memset(&g_dirent, 0, sizeof(SceIoDirent));
    
    SceUID dfd = sceIoDopen(directory);
    
    if (dfd < 0)
        return dfd;
    
    // read every entry
    while (sceIoDread(dfd, &g_dirent) > 0)
    {
        // skip past "." and ".."
        if (strcmp(g_dirent.d_name, ".") == 0 || strcmp(g_dirent.d_name, "..") == 0)
            continue;
        
        // rebuild full name
        strcpy(newfile, directory);
        strcat(newfile, "/");
        strcat(newfile, g_dirent.d_name);
        
        // if directory, we descend
        if (g_dirent.d_stat.st_mode & FIO_S_IFDIR)
        {
            int res = ReadDirectory(newfile, handler);

            if (res < 0)
            {
                sceIoDclose(dfd);
                return res;
            }
        }
        else
        {
            int res = handler(newfile);

            if (res < 0)
            {
                sceIoDclose(dfd);
                return res;
            }
        }
    }
    
    sceIoDclose(dfd);
    return 0;
}

int VerifyFileSHA1(const char *filename, const unsigned char *check, u8 *work_buffer, int size)
{
	int read;
	u8 sha1[20];
	SceKernelUtilsSha1Context ctx;

	SceUID fd = sceIoOpen(filename, PSP_O_RDONLY, 0);

	if (fd < 0)
	{
        return -1;
	}

	sceKernelUtilsSha1BlockInit(&ctx);

	while ((read = sceIoRead(fd, work_buffer, size)) > 0)
	{
		sceKernelUtilsSha1BlockUpdate(&ctx, work_buffer, read);
	}

	sceKernelUtilsSha1BlockResult(&ctx, sha1);
	sceIoClose(fd);

	if (memcmp(sha1, check, 20) != 0)
	{
        return -2;
	}
    
    return 0;
}

int LoadStartModule(const char *filename)
{
    SceUID mod = sceKernelLoadModule(filename, 0, NULL);
    
    if (mod < 0)
    {
        return mod;
    }
    
    return sceKernelStartModule(mod, 0, NULL, NULL, NULL);
}

int isPspGo(void)
{
    return getTrueModel() == 4 ? (1) : (0);
}
