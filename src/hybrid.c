/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "hybrid.h"
#include "progressbar.h"
#include "vlfutility.h"
#include "utility.h"
#include "updater.h"
#include "psar.h"
#include "subset631.h"
#include "firmware661.h"

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspctrl.h>

#include <string.h>

#include <vlf.h>
#include <pspdecrypt.h>
#include <infinity_kinstaller.h>
#include <customfile.h>

// modules to include
#include <modules/pspdecrypt_prx.h>
#include <modules/infinity_kinstaller_prx.h>
#include <modules/psar_driver_prx.h>

// custom firmware files
#include <core/bootloaderfiles.h>
#include <core/coreflashfiles.h>
#include <661/subset661flashfiles.h>

void *malloc64(int size);

const unsigned char g_631_update_sha1[2][0x14] =
{
    // normal updater
    {
        0x5C, 0x61, 0x6D, 0xCF, 0x04, 0x9D, 0x07, 0x07, 0xC0, 0xC8,
        0x3E, 0xA3, 0x91, 0x10, 0xFE, 0x4F, 0x38, 0x4E, 0x72, 0x2D
    },

    // go updater
    {
        0x78, 0x9D, 0x41, 0xC1, 0xAC, 0xE3, 0x46, 0x3C, 0x86, 0x25,
        0x83, 0x66, 0x49, 0x07, 0x54, 0x19, 0xB3, 0x02, 0x79, 0xC6
    }
};

const unsigned char g_661_update_sha1[2][0x14] =
{
    // normal updater
    {
        0xBC, 0x10, 0xAD, 0x21, 0x14, 0xE1, 0xC3, 0x49, 0xF2, 0x3E,
        0xE2, 0xB1, 0x30, 0xD8, 0x2F, 0x7E, 0x86, 0x34, 0x60, 0xAC
    },

    // go updater
    {
        0x91, 0xF7, 0x8E, 0xCE, 0xC0, 0x25, 0x70, 0x5F, 0x38, 0xC3,
        0x68, 0x80, 0x59, 0xCB, 0x14, 0xFF, 0x42, 0x57, 0xAB, 0x8A
    }
};


#define DATA_BUFFER_SIZE    (10*1024*1024)
static u8 *g_databuffer;

static u8 got_iplupdate = 0, got_iplupdate3g = 0, got_emcsm = 0, got_lfashfatfmt = 0, got_lfatfs = 0;

extern char g_local_path[];

const char *updatePath(void)
{
    return g_local_path;
}

static int get_available_models(void)
{
    return isPspGo() ? MODEL(4) : (ALL_MODELS & ~MODEL(4));
}

int OnInstallComplete(void *param)
{
    vlfGuiSetModelSpeed(1.f/60.f);

    RemoveProgressBar(0);
	vlfGuiChangeCharacterByButton('*', VLF_ENTER);
    SetProgressBarStatus("Complete. Press * to exit.");

    vlfGuiAddEventHandler(PSP_CTRL_CROSS, 0, (int (* )(void *param))sceKernelExitGame, NULL);
	return VLF_EV_RET_REMOVE_HANDLERS;
}

static void load_kernel_modules(void)
{
    SetProgressBarStatus("Loading kernel modules...");

    // write our kernel helpers
    int res = WriteFile("pspdecrypt.prx", pspdecrypt_prx, size_pspdecrypt_prx);

    if (res != size_pspdecrypt_prx)
    {
        ErrorExit("Error 0x%08X writing pspdecrypt.", res);
    }

    res = WriteFile("infinity_kinstaller.prx", infinity_kinstaller_prx, size_infinity_kinstaller_prx);

    if (res != size_infinity_kinstaller_prx)
    {
        ErrorExit("Error 0x%08X writing infinity_kinstaller.", res);
    }

    res = WriteFile("psar_driver.prx", psar_driver_prx, size_psar_driver_prx);

    if (res != size_psar_driver_prx)
    {
        ErrorExit("Error 0x%08X writing psar_driver.", res);
    }

    // load our kernel helpers
    SceUID modid = LoadStartModule("pspdecrypt.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading pspdecrypt.", modid);
    }

    modid = LoadStartModule("infinity_kinstaller.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading infinity kernel module.", modid);
    }

    modid = LoadStartModule("psar_driver.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading psar_driver.", modid);
    }
}

static void create_directories(void)
{
    const char *directories[] =
    {
        "flash0:/codepage",
        "flash0:/data",
        "flash0:/data/cert",
        "flash0:/dic",
        "flash0:/font",
        "flash0:/kd",
        "flash0:/kd/resource",
        "flash0:/kn",
        "flash0:/kn/resource",
        "flash0:/vsh",
        "flash0:/vsh/etc",
        "flash0:/vsh/module",
        "flash0:/vsh/resource",
        "flash0:/vsn",
        "flash0:/vsn/etc",
        "flash0:/vsn/module",
        "flash0:/vsn/resource",
    };

    SetProgressBarStatus("Creating directories...");

    if (mfcInitSection("DIR") < 0)
	{
		ErrorExit("Error creating \"DIR\" section.");
	}

    int i;
	for (i = 0; i < sizeof(directories)/sizeof(char *); i++)
	{
		if (mfcAddDirectory((char *)directories[i], ALL_MODELS) < 0)
		{
			ErrorExit("Error adding directory \"%s\".", directories[i]);
		}
	}

	if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"DIR\" section.");
	}
}

static void add_updater_prx(const char *filename, PSP_Header *prx, int model)
{
    int size = pspDecryptPRX((u8 *)prx, (u8 *)prx, prx->psp_size);

    if (size < 0)
    {
        ErrorExit("Error 0x%08X decrypting \"%s\".", size, filename);
    }

    int res = mfcAddFile((char *)filename, (u8 *)prx, size, model, 0);

    if (res < 0)
    {
        ErrorExit("Error %i adding \"%s\" to archive.", res, filename);
    }
}

static HandlingResult updater_prx_extraction_handler(PSP_Header *prx, unsigned int size, HidingMechanism mechanism)
{
    if (strcmp(prx->modname, "TexSeqCounter") == 0 && mechanism == MODE_ENCRYPTED)
    {
        // this is is 03g+ ipl_update.prx
        add_updater_prx("ipl_update.prx", prx, get_available_models() & ~(MODEL(0) | MODEL(1)));
        got_iplupdate3g = 1;
    }
    else if (strcmp(prx->modname, "TexSeqCounter") == 0 && mechanism != MODE_ENCRYPTED)
    {
        // this is 01g/02g (or 05g) ipl_update.prx
        add_updater_prx("ipl_update.prx", prx, get_available_models() & (MODEL(0) | MODEL(1) | MODEL(4)));
        got_iplupdate = 1;
    }
    else if (strcmp(prx->modname, "sceLflashFatfmtUpdater") == 0)
    {
        add_updater_prx("lflash_fatfmt.prx", prx, get_available_models());
        got_lfashfatfmt = 1;
    }
    else if (strcmp(prx->modname, "sceLFatFs_Updater_Driver") == 0)
    {
        add_updater_prx("lfatfs_updater.prx", prx, get_available_models());
        got_lfatfs = 1;
    }
    else if (strcmp(prx->modname, "sceNAND_Updater_Driver") == 0)
    {
        add_updater_prx("emc_sm_updater.prx", prx, get_available_models());
        got_emcsm = 1;
    }

    return CONTINUE;
}

static void extract_updater_prx(const char *filename)
{
    SetProgressBarStatus("Extracting special updater prx...");

    if (mfcInitSection("UPD") < 0)
	{
		ErrorExit("Error creating \"UPD\" section.");
	}

    // extract the updater prx from SCE updater
    int res = ExtractUpdaterPRX(filename, (char *)g_databuffer, DATA_BUFFER_SIZE, updater_prx_extraction_handler);

    if (res < 0)
    {
        ErrorExit("Error 0x%08X extracting updater prx files.");
    }

	if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"UPD\" section.");
	}

    if (isPspGo())
    {
        if (!got_iplupdate || !got_lfashfatfmt || !got_lfatfs || !got_emcsm)
        {
            ErrorExit("Did not find all required updater modules.");
        }
    }
    else
    {
        if (!got_iplupdate || !got_lfashfatfmt || !got_lfatfs || !got_emcsm || !got_iplupdate3g)
        {
            ErrorExit("Did not find all required updater modules.");
        }
    }
}

static void PackBootloader(int start_percentage, int end_percentage, u8 *buffer, int size)
{
    SetProgressBarStatus("Packing bootloader...");

 	if (mfcInitSection("BL") < 0)
	{
		ErrorExit("Error creating \"BL\" section.");
	}

    int res = MountPSAR("631.PBP");

    if (res < 0)
    {
        ErrorExit("Error 0x%08X mounting 6.31 updater.", res);
    }

    char *files[] =
    {
        "psar0:/flash0/kd/systimer.prx",
        "psar0:/flash0/kd/init.prx"
    };

    int i;
    for (i = 0; i < BOOTLOADER_FILE_N; ++i)
    {
        int res = mfcAddFile(g_bootloader_files[i].path, g_bootloader_files[i].buffer, g_bootloader_files[i].size, ALL_MODELS, 0);

        if (res < 0)
        {
            ErrorExit("Error 0x%08X adding file '%s'.", res, g_bootloader_files[i].path);
        }

        SetProgressBarPercentage(start_percentage+(((end_percentage-start_percentage)*i)/(BOOTLOADER_FILE_N+(sizeof(files)/sizeof(char *)))), 0);
    }

    for (i = 0; i < sizeof(files)/sizeof(char *); ++i)
    {
        int read_size = ReadFile(files[i], 0, buffer, size);

        if (read_size < 0)
        {
            ErrorExit("Error 0x%08X reading file '%s'.", read_size, files[i]);
        }

        int res = mfcAddFile(strrchr(files[i], '/')+1, buffer, read_size, ALL_MODELS, 0);

        if (res < 0)
        {
            ErrorExit("Error 0x%08X adding file '%s'.", res, files[i]);
        }
        SetProgressBarPercentage(start_percentage+(((end_percentage-start_percentage)*(i+BOOTLOADER_FILE_N))/(BOOTLOADER_FILE_N+(sizeof(files)/sizeof(char *)))), 0);
    }

    res = UnmountPSAR();

    if (res < 0)
    {
        ErrorExit("Error 0x%08X unmounting 6.31 updater.", res);
    }

    if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"BL\" section.");
	}
}

static void PackCustomFirmware(int start_percentage, int end_percentage, u8 *buffer, int size)
{
    SetProgressBarStatus("Packing 6.61 firmware controller...");

 	if (mfcInitSection("CFW") < 0)
	{
		ErrorExit("Error creating \"CFW\" section.");
	}

    int i;
    for (i = 0; i < CORE_FLASH_FILE_N; ++i)
    {
        int res = mfcAddFile(g_core_files[i].path, g_core_files[i].buffer, g_core_files[i].size, ALL_MODELS, 0);

        if (res < 0)
        {
            ErrorExit("Error 0x%08X adding file '%s'.", res, g_core_files[i].path);
        }

        SetProgressBarPercentage(start_percentage+(((end_percentage-start_percentage)*(i))/(CORE_FLASH_FILE_N+SUBSET661_FLASH_FILE_N)), 0);
    }

    for (i = 0; i < SUBSET661_FLASH_FILE_N; ++i)
    {
        int res = mfcAddFile(g_subset661_files[i].path, g_subset661_files[i].buffer, g_subset661_files[i].size, ALL_MODELS, 0);

        if (res < 0)
        {
            ErrorExit("Error 0x%08X adding file '%s'.", res, g_subset661_files[i].path);
        }

        SetProgressBarPercentage(start_percentage+(((end_percentage-start_percentage)*(i+CORE_FLASH_FILE_N))/(CORE_FLASH_FILE_N+SUBSET661_FLASH_FILE_N)), 0);
    }

    if (mfcEndSection() < 0)
	{
		ErrorExit("Error ending \"CFW\" section.");
	}
}

void BuildHybridFirmware(void)
{
    // allocate memory
    g_databuffer = malloc64(DATA_BUFFER_SIZE);

    if (g_databuffer == NULL)
    {
        ErrorExit("Not enough memory for internal buffers!");
    }

    // move to UPDATE directory
    sceIoChdir(updatePath());

    SetProgressBarPercentage(0, 1);

    // begin loading kernel modules
    load_kernel_modules();
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(1, 1);

    // verify 6.31 updater
    SetProgressBarStatus("Verifying 6.31 updater...");
    int res = VerifyFileSHA1("631.PBP", g_631_update_sha1[isPspGo()], g_databuffer, DATA_BUFFER_SIZE/8);

    if (res < 0)
    {
        ErrorExit("Error 0x%08X verifying 6.31 updater. Ensure updater is for the correct model, version and is stored in the correct place.", res);
    }

    SetProgressBarPercentage(5, 1);

    // verify 6.61 updater
    SetProgressBarStatus("Verifying 6.61 updater...");
    res = VerifyFileSHA1("661.PBP", g_661_update_sha1[isPspGo()], g_databuffer, DATA_BUFFER_SIZE/8);

    if (res < 0)
    {
        ErrorExit("Error 0x%08X verifying 6.61 updater. Ensure updater is for the correct model, version and is stored in the correct place.", res);
    }

    SetProgressBarPercentage(10, 1);

    // initialise archive
    if (mfcInit("DATA.MFC") < 0)
    {
        ErrorExit("Error initialising firmware container.");
    }

    // create directories
    create_directories();
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(11, 1);

    // extract updater prx
    extract_updater_prx("631.PBP");
    SetProgressBarPercentage(15, 1);

    // pack 6.31 subset
    PackSubset(15, 30, g_databuffer, DATA_BUFFER_SIZE);
    SetProgressBarPercentage(30, 1);

    // pack 6.61 firmware
    PackFirmware(30, 95, g_databuffer, DATA_BUFFER_SIZE);
    SetProgressBarPercentage(95, 1);

    // pack bootloader files
    PackBootloader(95, 98, g_databuffer, DATA_BUFFER_SIZE);
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(98, 1);

    // pack controller for 6.61 firmware
    PackCustomFirmware(98, 100, g_databuffer, DATA_BUFFER_SIZE);
    sceKernelDelayThread(1200000);
    SetProgressBarPercentage(100, 1);

    if (mfcEnd() < 0)
	{
		ErrorExit("Error ending firmware archive.");
	}

    vlfGuiAddEventHandler(0, 600000, OnInstallComplete, NULL);
    sceKernelExitDeleteThread(0);
}
