/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#ifndef REBOOT_H_
#define REBOOT_H_

#include <psptypes.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef enum 
{ 
    REBOOT_NO_ERR, 
    REBOOT_READ_ERR, 
    REBOOT_DECRYPT_EXEC_ERR, 
    REBOOT_DECOMPRESS_EXEC_ERR,
    REBOOT_FIND_ERR,
    REBOOT_DECRYPT_ERR,
    REBOOT_DECOMPRESS_ERR
} RebootErrors;

typedef void (* REBOOT_EXTRACTION_HANDLER)(const char *filename, const u8 *reboot_bin, unsigned int size);

RebootErrors ExtractReboot(const char *filename, u8 *work_buffer, int size, REBOOT_EXTRACTION_HANDLER handler);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // REBOOT_H_
