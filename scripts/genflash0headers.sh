#!/bin/bash

# Copyright (C) 2015, David "Davee" Morgan

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

FILES=$(tree -ifa --noreport flash0)
flash0_headers=$(mktemp /tmp/flash0_headers.XXXXXX)
flash0_files=$(mktemp /tmp/flash0_files.XXXXXX)

echo "#ifndef ${1^^}FLASHFILES_H
#define ${1^^}FLASHFILES_H
" >> $flash0_headers

echo "#include <customfile.h>
" >> $flash0_headers

#echo "typedef struct
##{
#    const char *path;
#    const char *data;
#    unsigned int size;
#} FlashFile;
#" > "$flash0_files"

echo "CustomFile g_${1,,}_files[] =
{" >> "$flash0_files"

for f in $FILES; do
    if [ -f $f ]; then
        real="flash0:/"${f#"flash0/"}
        name=${f/"."/"_"}
        name=${name##*/}
        header=${name}".h"
        bin2c $f $header $name
        printf '#include "%s"\n' $header >> "$flash0_headers"
        printf '\t{"%s", %s, sizeof(%s) },\n' $real $name $name >> "$flash0_files"
    fi
done

printf '\n' >> "$flash0_headers"
printf "};\n" >> "$flash0_files"
printf "\n#define ${1^^}_FLASH_FILE_N\t(sizeof(g_${1,,}_files)/sizeof(CustomFile))\n" >> $flash0_files
printf "\n#endif // ${1^^}FLASHFILES_H\n" >> $flash0_files
cat $flash0_headers $flash0_files > "${1,,}flashfiles.h"

rm $flash0_headers
rm $flash0_files
