#!/bin/bash

# Copyright (C) 2015, David "Davee" Morgan

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

FILES=$(tree -ifa --noreport bootloader)
headers=$(mktemp /tmp/headers.XXXXXX)
files=$(mktemp /tmp/files.XXXXXX)

echo "#ifndef ${1^^}FILES_H
#define ${1^^}FILES_H
" >> $headers

echo "#include <customfile.h>
" >> $headers

echo "CustomFile g_${1,,}_files[] =
{" >> "$files"

for f in $FILES; do
    if [ -f $f ]; then
        name=${f/"."/"_"}
        name=${name##*/}
        header=${name}".h"
        bin2c $f $header $name
        printf '#include "%s"\n' $header >> "$headers"
        printf '\t{"%s", %s, sizeof(%s) },\n' ${f##*/} $name $name >> "$files"
    fi
done

printf '\n' >> "$headers"
printf "};\n" >> "$files"
printf "\n#define ${1^^}_FILE_N\t(sizeof(g_${1,,}_files)/sizeof(CustomFile))\n" >> $files
printf "\n#endif // ${1^^}FILES_H\n" >> $files

cat $headers $files > "${1,,}files.h"

rm $headers
rm $files
