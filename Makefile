TARGET = maker
OBJS = src/crt0.o src/main.o src/updater.o src/reboot.o src/utility.o src/psar.o src/subset631.o \
        src/firmware661.o src/progressbar.o src/vlfutility.o src/hybrid.o src/pspDecrypt.o \
        src/infinity_kinstaller.o

INCDIR = include .
CFLAGS =  -std=c99 -fno-builtin -O2 -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS) -c

LIBDIR = lib
LDFLAGS = -nostdlib -nodefaultlibs
LIBS = -lpspvshbridge -lpsppower -lz -lvlfgui -lvlfgu -lvlfutils -lvlflibc

BUILD_PRX = 1
USE_KERNEL_LIBS = 0
USE_PSPSDK_LIBC = 1

EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_ICON = assets/icon0.png
PSP_EBOOT_TITLE = 6.61 Infinity Firmware Builder

PSPSDK=$(shell psp-config --pspsdk-path)
include src/build.mak
